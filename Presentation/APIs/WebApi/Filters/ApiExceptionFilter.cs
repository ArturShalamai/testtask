﻿using Application.Features.Record_Import.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;
using System.Threading.Tasks;
using WebApi.DTO;

namespace WebApi.Filters
{
    /// <summary>
    /// Filters and returns safe exception to client.
    /// </summary>
    public class ApiExceptionFilter : ExceptionFilterAttribute
    {
        /// <summary>
        /// Handles application exceptions in async manner.
        /// </summary>
        /// <param name="context"><see cref="ExceptionContext"/>.</param>
        /// <returns>Returns <see cref="Task"/>.</returns>
        public override Task OnExceptionAsync(ExceptionContext context)
        {
            var exception = context.Exception;

            // Loop through all inner exceptions.
            var innerException = exception.InnerException;
            while (innerException != null)
            {
                exception = innerException;
                innerException = exception.InnerException;
            }

            Log.Logger.Error(exception, "Exception was processed inside excpetion filter.");

            ApiError apiError = null;
            if (exception is ApplicationException)
            {
                // Handle explicit 'known' API errors.
                var ex = exception as ApplicationException;
                context.Exception = null;
                apiError = new ApiError(ex.Message);

                // Send user error code.
                context.HttpContext.Response.StatusCode = 400;
            }
            if(exception is InvalidImportFileException)
            {
                // Handle explicit 'known' API errors.
                var ex = exception as InvalidImportFileException;
                context.Exception = null;
                apiError = new ApiError(ex.Message)
                {
                    Detail = ex.FileErrors
                };

                // Send user error code.
                context.HttpContext.Response.StatusCode = 400;
            }
            else
            {
                // Unhandled errors.
#if !DEBUG
                var msg = "An unhandled error occurred.";                
                string stack = null;
#else
                // If in debug, include also an stack trace as part of an detail.
                var msg = exception.GetBaseException().Message;
                string stack = exception.StackTrace;
#endif
                
                apiError = new ApiError(msg);
                apiError.Detail = stack;

                // Send server error code.
                context.HttpContext.Response.StatusCode = 500;
            }

            // always return a JSON result
            context.Result = new JsonResult(apiError);

            return base.OnExceptionAsync(context);
        }

    }
}
