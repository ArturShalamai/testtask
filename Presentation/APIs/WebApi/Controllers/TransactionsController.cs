﻿using Application.Features;
using Application.Features.Record_Import;
using Application.Features.Record_Import.Exceptions;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Configurations;
using WebApi.DTO;

namespace WebApi.Controllers
{
    /// <summary>
    /// Controller for transaction imports.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        /// <summary>
        /// Manager for transaction records.
        /// </summary>
        private readonly ITransactionRecordsManager _recordsManager;

        /// <summary>
        /// Transaction records import manager.
        /// </summary>
        private readonly IRecordsImportManager _recordsImportManager;

        /// <summary>
        /// Configuration for transaction records import.
        /// </summary>
        private readonly TransactionsImportSettings _importConfigs;

        /// <summary>
        /// AutoMapper.
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Initiates a new instance of <see cref="TransactionsController"/>.
        /// </summary>
        /// <param name="importConfigs"><see cref="IRecordsImportManager"/> to import transaction records.</param>
        /// <param name="recordsImportManager"><see cref="IRecordsImportManager"/> to perform import of transaction records.</param>
        /// <param name="recordsManager"><see cref="ITransactionRecordsManager"/> to perform operation on transaction records.</param>
        /// <param name="mapper">Mapper to map entities to DTOs.</param>
        public TransactionsController(IRecordsImportManager recordsImportManager, TransactionsImportSettings importConfigs, ITransactionRecordsManager recordsManager, IMapper mapper)
        {
            this._recordsImportManager = recordsImportManager ?? throw new ArgumentNullException(nameof(recordsImportManager));
            this._importConfigs = importConfigs ?? throw new ArgumentNullException(nameof(importConfigs));
            this._recordsManager = recordsManager ?? throw new ArgumentNullException(nameof(recordsManager));
            this._recordsManager = recordsManager ?? throw new ArgumentNullException(nameof(recordsManager));
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get all transaction records according to specified skip and limit.
        /// </summary>
        /// <returns>Returns <see cref="IEnumerable{TransactionRecord}"/> of found transaction Records.</returns>
        [HttpGet()]
        [ProducesResponseType(typeof(IEnumerable<TransactionRecordDto>), 200)]
        public async Task<IActionResult> GetTransactionRecords([FromQuery] int skip, [FromQuery] int limit)
        {
            // No entities to return, so don't need to waste any resources on this request.
            if (skip != 0 && limit == 0)
                return await Task.FromResult(Ok(Array.Empty<int>()));

            var result = await this._recordsManager.GetAllRecordsAsync(skip, limit);
            return Ok(result.Select(r => this._mapper.Map<TransactionRecordDto>(r)));
        }

        /// <summary>
        /// Get transaction records filtered by status.
        /// </summary>
        /// <returns>Returns <see cref="IEnumerable{TransactionRecord}"/> of found transaction Records.</returns>
        [HttpGet("{status:int}")]
        [ProducesResponseType(typeof(IEnumerable<TransactionRecordDto>), 200)]
        public async Task<IActionResult> GetTransactionRecords(Core.Enums.TransactionStatus status)
        {
            var result = await this._recordsManager.QueryRecordsaAsync(record => record.Status == status);
            return Ok(result.Select(r => this._mapper.Map<TransactionRecordDto>(r)));
        }

        /// <summary>
        /// Get transaction records filtered by currency code.
        /// </summary>
        /// <returns>Returns <see cref="IEnumerable{TransactionRecord}"/> of found transaction Records.</returns>
        [HttpGet("{currencyCode}")]
        [ProducesResponseType(typeof(IEnumerable<TransactionRecordDto>), 200)]
        public async Task<IActionResult> GetTransactionRecords(string currencyCode)
        {
            if (string.IsNullOrWhiteSpace(currencyCode))
                return BadRequest("Currency code was missing or invalid.");
            else if (currencyCode.Length != 3)
                return BadRequest("Currency code is invalid.");

            var result = await this._recordsManager.QueryRecordsaAsync(record => string.Equals(record.CurrencyCode, currencyCode));

            // Return mapped result.
            return Ok(result.Select(r => this._mapper.Map<TransactionRecordDto>(r)));
        }

        /// <summary>
        /// Get transaction records filtered by date range.
        /// </summary>
        /// <param name="startDate">Start date including.</param>
        /// <param name="endDate">End date including.</param>
        /// <returns>Returns <see cref="IEnumerable{TransactionRecord}"/> of found transaction Records.</returns>
        [HttpGet("{startDate:datetime}/{endDate:datetime}")]
        [ProducesResponseType(typeof(IEnumerable<TransactionRecordDto>), 200)]
        public async Task<IActionResult> GetTransactionRecords(DateTime startDate, DateTime endDate)
        {
            var result = await this._recordsManager.QueryRecordsaAsync(record => record.TransactionDate >= startDate && record.TransactionDate <= endDate);
            return Ok(result.Select(r => this._mapper.Map<TransactionRecordDto>(r)));
        }

        /// <summary>
        /// Imports transaction records from provided file.
        /// </summary>
        /// <param name="uploadedFile"><see cref="IFormFile"/> of file to import.</param>
        /// <returns>Returns <see cref="IActionResult"/> with operation result.</returns>
        [HttpPost("records")]
        [ProducesResponseType(200)]
        [ProducesErrorResponseType(typeof(ApiError))]
        public async Task<IActionResult> UploadRecordsFromFiles([FromForm] IFormFile uploadedFile)
        {
                // Validate input file.
                if (uploadedFile is null)
                    return await Task.FromResult(BadRequest("Transaction records file is missing."));

                // Validate size, should not be more than 1 MB.
                const int MBSize = 1024;
                if (uploadedFile.Length > this._importConfigs.MaxFileSize * MBSize)
                    return await Task.FromResult(BadRequest("Only file with size up to 1 Mb is allowed."));

                var format = Path.GetExtension(uploadedFile.FileName).ToLowerInvariant();

                if (!this._importConfigs.SupportedTypes.Contains(format))
                    return await Task.FromResult(BadRequest("Unknown format"));

                // Import transaction records.
                using (var str = uploadedFile.OpenReadStream())
                    await _recordsImportManager.FromStreamAsync(new StreamReader(str), format);

            // Return OK status.
            return await Task.FromResult(Ok());
        }
    }
}
