﻿using AutoMapper;
using Core.Models;
using WebApi.DTO;

namespace WebApi.Mappings
{
    /// <summary>
    /// Defines web api mappings between transaction records and their return dtos.
    /// </summary>
    public class TransactionRecordsProfile : Profile
    {
        /// <summary>
        /// Defines mappings.
        /// </summary>
        public TransactionRecordsProfile()
        {
            CreateMap<TransactionRecord, TransactionRecordDto>()
                .ForMember(src => src.Payment, opts => opts.MapFrom(src => $"{src.Amount} {src.CurrencyCode}"))
                .ForMember(src => src.Status, opts => opts.MapFrom(src => src.Status.ToString()));
        }
    }
}
