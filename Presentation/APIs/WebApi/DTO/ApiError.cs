﻿namespace WebApi.DTO
{
    /// <summary>
    /// Represent DTO for generic API problem.
    /// </summary>
    public class ApiError
    {
        /// <summary>
        /// Describes problem.
        /// </summary>
        public string Message { get; set; }
        
        /// <summary>
        /// Defines whether it was an consumer error.
        /// </summary>
        public bool IsError { get; set; }
        
        /// <summary>
        /// Hold information about details for of this exception,
        /// </summary>
        public object Detail { get; set; }

        /// <summary>
        /// Initiates a new instance of <see cref="ApiError"/>.
        /// </summary>
        /// <param name="message"></param>
        public ApiError(string message)
        {
            this.Message = message;
            this.IsError = true;
        }
    }
}
