﻿using Newtonsoft.Json;

namespace WebApi.DTO
{
    /// <summary>
    /// Dto for representing transaction record.
    /// </summary>
    public class TransactionRecordDto
    {
        /// <summary>
        /// Id of transaction record.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Transaction payment, represents "Amount Currency".
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Payment { get; set; }

        /// <summary>
        /// Transaction status.
        /// </summary>
        public string Status { get; set; }
    }
}
