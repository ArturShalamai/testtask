﻿using System.Collections.Generic;

namespace WebApi.Configurations
{
    /// <summary>
    /// Defines configuration for transaction records importings.
    /// </summary>
    public class TransactionsImportSettings
    {
        /// <summary>
        /// Maximum byte size for transaction records import file. 
        /// </summary>
        public long MaxFileSize { get; set; }

        /// <summary>
        /// List of supported file types.
        /// </summary>
        public List<string> SupportedTypes { get; set; }
    }
}
