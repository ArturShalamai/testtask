using Application.Features;
using Application.Features.Record_Import;
using Application.Features.Record_Import.CSV_Import;
using Application.Features.Record_Import.Validators;
using Application.Features.Record_Import.XML_Import;
using AutoMapper;
using Core.Interfaces;
using Core.Models;
using DataAccess.EntityFrameworkCore;
using DataAccess.EntityFrameworkCore.Storages;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using WebApi.Configurations;
using WebApi.Filters;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add controllers.
            // Add exception filters.
            services.AddControllers(cfg => cfg.Filters.Add(new ApiExceptionFilter()));

            // Add Swagger.
            services.AddSwaggerGen(c =>
            {
                c.IncludeXmlComments($@"{AppDomain.CurrentDomain.BaseDirectory}\WebApi.xml");
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Version = "v1",
                    Title = "Transaction records swagger endpoints"
                });
            });

            // Add Entity Framework Core. Configure Migrations.
            services.AddDbContext<ApplicationDbContext>(opts =>
                opts.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                sqlServerOpts => sqlServerOpts.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));

            // Add Automapper.
            services.AddAutoMapper(typeof(Startup), typeof(RecordsImportManager));

            // Add services.
            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());
            services.AddScoped<IGenericStorage<TransactionRecord>, TransactionRecordsStorage>();
            services.AddScoped<IRecordsImportManager, RecordsImportManager>();
            services.AddScoped<ITransactionRecordsManager, TransactionRecordsManager>();

            services.AddScoped<IXMLRecordsParser, XMLRecordsParser>();
            services.AddScoped<ICSVRecordsParser, CSVRecordsParser>();

            services.AddTransient<IValidator<TransactionRecord>, TransactionRecordValidator>();

            // Add configurations.
            var transactionsImportSettings = this.Configuration.GetSection("Transactions").Get<TransactionsImportSettings>();
            services.AddSingleton(typeof(TransactionsImportSettings), transactionsImportSettings);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Add Swagger
            app.UseSwagger();

            app.UseSwaggerUI(conf => conf.SwaggerEndpoint("/swagger/v1/swagger.json", "Transaction Records Web Api"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
