﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Application.Features
{
    /// <summary>
    /// Defines interface for generic operations on transaction records.
    /// </summary>
    public interface ITransactionRecordsManager
    {
        /// <summary>
        /// Returns all transaction records asynchronously. 
        /// </summary>
        /// <param name="skip">Number of entities to skip.</param>
        /// <param name="limit">Number of entities to limit.</param>
        /// <returns>Returns <see cref="IEnumerable{TransactionRecord}"/>.</returns>
        Task<IEnumerable<TransactionRecord>> GetAllRecordsAsync(int skip, int limit);

        /// <summary>
        /// Query transaction records according to specific filter.
        /// </summary>
        /// <param name="searchCriteria">Criteria to search.</param>
        /// <returns>Returns <see cref="IEnumerabe{TransactionRecord}"/>that can further be processed.</returns>
        Task<IEnumerable<TransactionRecord>> QueryRecordsaAsync(Expression<Func<TransactionRecord, bool>> searchCriteria);
    }
}