﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Application.Features
{
    /// <summary>
    /// Manages operations with transaction records.
    /// </summary>
    public class TransactionRecordsManager : ITransactionRecordsManager
    {
        /// <summary>
        /// Persistent storage for records.
        /// </summary>
        private readonly IGenericStorage<TransactionRecord> _storage;

        /// <summary>
        /// Initiates a new instance of <see cref="TransactionRecordsManager"/>.
        /// </summary>
        /// <param name="storage">Transaction records storage.</param>
        public TransactionRecordsManager(IGenericStorage<TransactionRecord> storage) =>
            this._storage = storage ?? throw new ArgumentNullException(nameof(storage));

        /// <inheritdoc>
        public async Task<IEnumerable<TransactionRecord>> GetAllRecordsAsync(int skip, int limit) =>
            await _storage.GetAllAsync(skip, limit);

        /// <inheritdoc>
        public async Task<IEnumerable<TransactionRecord>> QueryRecordsaAsync(Expression<Func<TransactionRecord, bool>> searchCriteria) =>
            await _storage.FindByAsync(searchCriteria);
    }
}
