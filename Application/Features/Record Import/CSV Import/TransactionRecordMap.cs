﻿using Core.Enums;
using Core.Models;
using CsvHelper.Configuration;
using System;
using System.Globalization;

namespace Application.Features.Record_Import.CSV_Import
{
    /// <summary>
    /// Defines csv file mappings for <see cref="TransactionRecord"/>.
    /// </summary>
    public class TransactionRecordMap : ClassMap<TransactionRecord>
    {
        /// <summary>
        /// Initialize a new instance of <see cref="TransactionRecordMap"/>, define <see cref="TransactionRecord"/> to csv mappings
        /// </summary>
        public TransactionRecordMap()
        {
            Map(r => r.Id).Index(0);
            Map(r => r.Amount).Index(1);
            Map(r => r.CurrencyCode).Index(2);
            Map(r => r.TransactionDate).Index(3).ConvertUsing(row => DateTime.ParseExact(row.GetField(3), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture));
            Map(r => r.Status).Index(4).ConvertUsing(row => this.MapTransactionStatus(row.GetField(4)));
        }

        /// <summary>
        /// Map CSV record status into <see cref="TransactionStatus"/>.
        /// </summary>
        /// <param name="reocrdStatus">Status inside CSV transaction record.</param>
        /// <returns>Returns parsed <see cref="TransactionStatus"/>.</returns>
        private TransactionStatus MapTransactionStatus(string reocrdStatus)
        {
            switch (reocrdStatus)
            {
                case "Approved":
                    return TransactionStatus.A;
                case "Failed":
                    return TransactionStatus.R;
                case "Finished":
                    return TransactionStatus.D;
                default:
                    throw new ArgumentException("Unsupported value.");
            }
        }
    }
}
