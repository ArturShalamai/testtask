﻿using Core.Models;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Application.Features.Record_Import.CSV_Import
{
    /// <summary>
    /// Defines methods for transaction records parsing from CSV files.
    /// </summary>
    public interface ICSVRecordsParser
    {
        /// <summary>
        /// Parse transaction records from CSV stream.
        /// </summary>
        /// <param name="stream">Stream with data.</param>
        /// <returns>Returns parsed transactions records.</returns>
        Task<IEnumerable<TransactionRecord>> ParseAsync(StreamReader stream);
    }
}
