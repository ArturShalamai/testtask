﻿using Core.Models;
using CsvHelper;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Features.Record_Import.CSV_Import
{
    /// <summary>
    /// <see cref="TransactionRecord"/> from CSV file parser.
    /// </summary>
    public class CSVRecordsParser : ICSVRecordsParser
    {
        /// <inheritdoc />
        public async Task<IEnumerable<TransactionRecord>> ParseAsync(StreamReader stream)
        {
            using (var csv = new CsvReader(stream, CultureInfo.InvariantCulture))
            {
                csv.Configuration.RegisterClassMap<TransactionRecordMap>();
                csv.Configuration.HasHeaderRecord = false;

                return await Task.FromResult(csv.GetRecords<TransactionRecord>().ToList());
            }
        }
    }
}
