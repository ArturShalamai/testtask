﻿using Core.Models;
using FluentValidation;

namespace Application.Features.Record_Import.Validators
{
    /// <summary>
    /// Validator for <see cref="TransactionRecord"/>
    /// </summary>
    public class TransactionRecordValidator : AbstractValidator<TransactionRecord>
    {
        /// <summary>
        /// Defines validation rules for entity.
        /// </summary>
        public TransactionRecordValidator()
        {
            RuleFor(record => record.Id).NotEmpty();
            RuleFor(record => record.Id).MaximumLength(50);

            RuleFor(record => record.CurrencyCode).Length(3);

            RuleFor(record => record.Status).IsInEnum();
        }
    }
}
