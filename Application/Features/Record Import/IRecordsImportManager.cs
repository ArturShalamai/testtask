﻿using System.IO;
using System.Threading.Tasks;

namespace Application.Features.Record_Import
{
    /// <summary>
    /// Defines methods for transaction imports from files.
    /// </summary>
    public interface IRecordsImportManager
    {
        /// <summary>
        /// Import transaction records from a stream.
        /// </summary>
        /// <param name="stream"><see cref="StreamReader"/> with transaction records data.</param>
        /// <returns><see cref="Task"></see></returns>
        Task FromStreamAsync(StreamReader stream, string format);
    }
}