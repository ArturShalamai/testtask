﻿using System;
using System.Collections.Generic;

namespace Application.Features.Record_Import.Exceptions
{
    /// <summary>
    /// Represents import records file errors, per each entity.
    /// </summary>
    public class InvalidImportFileException : ApplicationException
    {
        /// <summary>
        /// List of entities validation errors, in format [entityId, entityErrors[]].
        /// </summary>
        public List<KeyValuePair<string, string[]>> FileErrors { get; }

        /// <summary>
        /// Initialize a new instance of <see cref="InvalidImportFileException"/>.
        /// </summary>
        /// <param name="_fileErrors">List of file records errors.</param>
        public InvalidImportFileException(string message, List<KeyValuePair<string, string[]>> _fileErrors)
            : base(message) =>
            this.FileErrors = _fileErrors;
    }
}
