﻿using System;

namespace Application.Features.Record_Import.Exceptions
{
    /// <summary>
    /// Generic model for application exceptions.
    /// </summary>
    public class ApplicationException : Exception
    {
        /// <summary>
        /// Initiates a new instance of <see cref="ApplicationException"/>.
        /// </summary>
        public ApplicationException() : base()
        {

        }

        /// <summary>
        /// Initiates a new instance of <see cref="ApplicationException"/>.
        /// </summary>
        /// <param name="message">Error message.</param>
        public ApplicationException(string message) : base(message)
        {

        }
    }
}
