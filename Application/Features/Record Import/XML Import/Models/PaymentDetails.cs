﻿using System;
using System.Xml.Serialization;

namespace Application.Features.Record_Import.XML_Import.Models
{
    [Serializable]
    public class PaymentDetails
    {
        [XmlElement("Amount")]
        public decimal Amount { get; set; }

        [XmlElement("CurrencyCode")]
        public string CurrencyCode { get; set; }
    }
}
