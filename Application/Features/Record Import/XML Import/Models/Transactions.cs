﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Application.Features.Record_Import.XML_Import.Models
{
    [Serializable()]
    [XmlRoot("Transactions")]
    public class Transactions
    {
        [XmlElement("Transaction")]
        public List<Transaction> AllTransactions { get; set; }
    }
}
