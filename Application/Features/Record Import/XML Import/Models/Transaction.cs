﻿using System;
using System.Xml.Serialization;

namespace Application.Features.Record_Import.XML_Import.Models
{
    [Serializable()]
    [XmlRoot("Transaction")]
    public class Transaction
    {
        [XmlAttribute("id")]
        public string TransactionId { get; set; }

        [XmlElement("TransactionDate")]
        public DateTime TransactionDate { get; set; }

        [XmlElement("PaymentDetails")]
        public PaymentDetails PaymentDetails { get; set; }

        [XmlElement("Status")]
        public string Status { get; set; }
    }
}
