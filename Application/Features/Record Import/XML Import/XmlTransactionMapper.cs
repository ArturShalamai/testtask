﻿using Application.Features.Record_Import.XML_Import.Models;
using AutoMapper;
using Core.Enums;
using Core.Models;
using System;

namespace Application.Features.Record_Import.XML_Import
{
    /// <summary>
    /// Profile that defines mappings between XML <see cref="Transaction"/> and <see cref="TransactionRecord"/>.
    /// </summary>
    class XmlTransactionMapper : Profile
    {
        /// <summary>
        /// Initialize new instance of <see cref="XmlTransactionMapper"/>, defines mapping rules.
        /// </summary>
        public XmlTransactionMapper()
        {
            CreateMap<Transaction, TransactionRecord>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.TransactionId))
                .ForMember(dest => dest.Amount, opts => opts.MapFrom(src => src.PaymentDetails.Amount))
                .ForMember(dest => dest.CurrencyCode, opts => opts.MapFrom(src => src.PaymentDetails.CurrencyCode))
                .ForMember(dest => dest.TransactionDate, opts => opts.MapFrom(src => src.TransactionDate))
                .ForMember(dest => dest.Status, opts => opts.MapFrom(src => this.MapTransactionStatus(src.Status)));
        }

        /// <summary>
        /// Map XML status record into transaction record status.
        /// </summary>
        /// <param name="xmlStatus">XML transaction status.</param>
        /// <returns>Returns parsed <see cref="TransactionStatus"/>.</returns>
        private TransactionStatus MapTransactionStatus(string xmlStatus)
        {
            switch (xmlStatus)
            {
                case "Approved":
                    return TransactionStatus.A;
                case "Rejected":
                    return TransactionStatus.R;
                case "Done":
                    return TransactionStatus.D;
                default:
                    throw new ArgumentException("Unsupported value.");
            }
        }

    }
}
