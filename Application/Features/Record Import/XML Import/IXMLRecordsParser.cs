﻿using Core.Models;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Application.Features.Record_Import.XML_Import
{
    /// <summary>
    /// Defines methods for transaction records parsing from XML files.
    /// </summary>
    public interface IXMLRecordsParser
    {
        /// <summary>
        /// Parse transaction records from XML stream.
        /// </summary>
        /// <param name="stream">Stream with data.</param>
        /// <returns>Returns parsed transactions records.</returns>
        Task<IEnumerable<TransactionRecord>> ParseAsync(StreamReader stream);
    }
}