﻿using Application.Features.Record_Import.XML_Import.Models;
using AutoMapper;
using Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Application.Features.Record_Import.XML_Import
{
    /// <summary>
    /// Transactions record import from xml file.
    /// </summary>
    public class XMLRecordsParser : IXMLRecordsParser
    {
        /// <summary>
        /// Mapper to map xml format record into <see cref="TransactionRecord"/>.
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Initialize a new instance of <see cref="XMLFileImportProceesor"/>.
        /// </summary>
        /// <param name="mapper">Mapper to convert xml to object.</param>
        public XMLRecordsParser(IMapper mapper) =>
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));

        /// <inheritdoc/>
        public async Task<IEnumerable<TransactionRecord>> ParseAsync(StreamReader stream)
        {
            var serializer = new XmlSerializer(typeof(Transactions));
            var fileRecords = (Transactions)serializer.Deserialize(stream);

            var parsedRecords = new List<TransactionRecord>(fileRecords.AllTransactions.Count());

            // Map xml record to transaction record.
            foreach (var xmlRecord in fileRecords.AllTransactions)
                parsedRecords.Add(this._mapper.Map<TransactionRecord>(xmlRecord));

            return await Task.FromResult(parsedRecords);
        }
    }
}
