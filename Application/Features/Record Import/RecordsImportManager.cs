﻿using Application.Features.Record_Import.CSV_Import;
using Application.Features.Record_Import.Exceptions;
using Application.Features.Record_Import.Validators;
using Application.Features.Record_Import.XML_Import;
using Core.Interfaces;
using Core.Models;
using FluentValidation;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Features.Record_Import
{
    /// <summary>
    /// Import transaction records from files.
    /// </summary>
    public class RecordsImportManager : IRecordsImportManager
    {
        //The reason for separation this from TransactionRecordsManager is different core reasons for them to evolve and just to keep code clean in further changes.

        /// <summary>
        /// Persistent storage for records.
        /// </summary>
        private readonly IGenericStorage<TransactionRecord> _storage;

        /// <summary>
        /// Parser for XML files.
        /// </summary>
        private readonly IXMLRecordsParser _xmlFileParser;

        /// <summary>
        /// Parser for CSV files.
        /// </summary>
        private readonly ICSVRecordsParser _csvRecordParser;

        /// <summary>
        /// Validator for <see cref="TransactionRecord"/>.
        /// </summary>
        private readonly IValidator<TransactionRecord> _transactionValidator;

        /// <summary>
        /// Create new instance of <see cref="RecordsImportManager"/>
        /// </summary>
        /// <param name="storage">Persistent storage for transaction records.</param>
        /// <param name="xmlFileParser">Parser for records inside XML files.</param>
        /// <param name="csvRecordParser">Parser for records inside CSV files.</param>
        /// <param name="transactionValidator">Validator for <see cref="TransactionRecord"/> entity.</param>
        public RecordsImportManager(IGenericStorage<TransactionRecord> storage, IXMLRecordsParser xmlFileParser, ICSVRecordsParser csvRecordParser, IValidator<TransactionRecord> transactionValidator)
        {
            this._storage = storage ?? throw new ArgumentNullException(nameof(storage));
            this._xmlFileParser = xmlFileParser ?? throw new ArgumentNullException(nameof(xmlFileParser));
            this._csvRecordParser = csvRecordParser ?? throw new ArgumentNullException(nameof(csvRecordParser));
            this._transactionValidator = transactionValidator ?? throw new ArgumentNullException(nameof(transactionValidator));
        }

        /// <inheritdoc/>
        public async Task FromStreamAsync(StreamReader stream, string format)
        {
            var fileRecords = new List<TransactionRecord>();

            var isFileValid = true;

            try
            {
                // Parse transaction records based on file type. If not supported throw an error.
                switch (format)
                {
                    case ".xml":
                        fileRecords = (await this._xmlFileParser.ParseAsync(stream)).ToList();
                        break;
                    case ".csv":
                        fileRecords = (await this._csvRecordParser.ParseAsync(stream)).ToList();
                        break;
                    default:
                        throw new ArgumentException("Unknown format.");
                }
            }
            catch (Exception ex)
            {
                // If something went wrong with parsing - infrom in generic manner;
                Log.Logger.Error(ex, "Failed parsing transaction records file.");
                throw new Exceptions.ApplicationException("Cannot process file. Invalid formatting.");
            }

            // Validate records and collect their validation errors.
            var validator = new TransactionRecordValidator();
            var fileErrors = new List<KeyValuePair<string, string[]>>(fileRecords.Count);

            foreach (var record in fileRecords)
            {
                var validationResult = this._transactionValidator.Validate(record);
                if (!validationResult.IsValid)
                {
                    // Add errors related to entities.
                    fileErrors.Add(new KeyValuePair<string, string[]>(record.Id,
                        validationResult.Errors.Select(x => x.ErrorMessage).ToArray()));

                    // If one record is invalid - entire file treated as invalid.
                    isFileValid = false;
                }
            }

            if (!isFileValid)
            {
                // Log failed transacrion records;
                Log.Logger.Error("Failed with processing such transaction records {@records}.", fileErrors);

                // Throw exception with list of records errors.
                throw new InvalidImportFileException("Transaction records are invalid.",fileErrors);
            }

            // If there was no errors, add or update records in persistent storage.
            var ids = fileRecords.Select(x => x.Id).ToList();

            var existentRecords = await this._storage.FindByAsync(record => ids.Contains(record.Id));
            var recordsToUpdate = fileRecords.Where(x => existentRecords.Any(y => string.Equals(x.Id, y.Id)));
            var newRecords = fileRecords.Where(x => !existentRecords.Any(y => string.Equals(x.Id, y.Id)));

            // Update existing records.
            if (existentRecords.Any())
                await this._storage.UpdateRangeAsync(recordsToUpdate);

            // Add new records.
            if(newRecords.Any())
                await this._storage.AddRangeAsync(newRecords);

            // Save into storage.
            await this._storage.SaveChangesAsync();
        }

    }
}
