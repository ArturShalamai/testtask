﻿using Core.Enums;
using System;

namespace Core.Models
{
    /// <summary>
    /// Model for information about transaction.
    /// </summary>
    public class TransactionRecord : Entity
    {
        /// <summary>
        /// Unique identifier of the entity. Max 50 chars.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Amount of the transaction.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Currency code in ISO4217 format.
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Date of the transaction.
        /// </summary>
        public DateTime TransactionDate { get; set; }

        /// <summary>
        /// Status of the transaction.
        /// </summary>
        public TransactionStatus Status { get; set; }
    }
}
