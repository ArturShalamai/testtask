﻿namespace Core.Enums
{
    /// <summary>
    /// Enum that holds transaction status
    /// </summary>
    public enum TransactionStatus
    {
        /// <summary>
        /// Approved
        /// </summary>
        A,
        
        /// <summary>
        /// Rejected
        /// </summary>
        R,
        
        /// <summary>
        /// Done
        /// </summary>
        D
    }
}
