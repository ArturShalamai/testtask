﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    /// <summary>
    /// Represents generic storage interface that data access providers must implement.
    /// </summary>
    public interface IGenericStorage<Entity>
    {
        /// <summary>
        /// Return <see cref="IEnumerable{Entity}"/> for all entities.
        /// </summary>
        /// <param name="skip">Defines number of entities to skip.</param>
        /// <param name="limit">Defines number of entities to return.</param>
        /// <returns>Returns <see cref="IEnumerable{Entity}"/>.</returns>
        Task<IEnumerable<Entity>> GetAllAsync(int skip, int limit);

        /// <summary>
        /// Returns <see cref="Entity"/> found via sppecified criteria.
        /// </summary>
        /// <param name="searchCriteria">Criteria to search.</param>
        /// <returns>Returns <see cref="Entity"/>.</returns>
        Task<Entity> FindOneAsync(Expression<Func<Entity, bool>> searchCriteria);

        /// <summary>
        /// Find specified entity.
        /// </summary>
        /// <param name="searchCriteria">Criteria to search.</param>
        /// <returns>Returns <see cref="IEnumerable{Entity}"/> that matched search criteria.</returns>
        Task<IEnumerable<Entity>> FindByAsync(Expression<Func<Entity, bool>> searchCriteria);

        /// <summary>
        /// Add new entity.
        /// </summary>
        /// <param name="newRecord">Entity to add.</param>
        Task AddAsync(Entity newRecord);

        /// <summary>
        /// Add range of new entities.
        /// </summary>
        /// <param name="newRecord">Entities to add.</param>
        Task AddRangeAsync(IEnumerable<Entity> newEntities);

        /// <summary>
        /// Update specified existing entity with provided data.
        /// </summary>
        /// <param name="updatedEntity">Updated entity.</param>
        Task UpdateAsync(Entity updatedEntity);

        /// <summary>
        /// Update existing entities with provided data.
        /// </summary>
        /// <param name="updatedEntities">Updated entities.</param>
        Task UpdateRangeAsync(IEnumerable<Entity> updatedEntities);

        /// <summary>
        /// Remove specific entity.
        /// </summary>
        /// <param name="removeCriteria">Criteria to remove.</param>
        Task RemoveAsync(Expression<Func<Entity, bool>> removeCriteria);

        /// <summary>
        /// Save changes.
        /// </summary>
        Task SaveChangesAsync();
    }
}
