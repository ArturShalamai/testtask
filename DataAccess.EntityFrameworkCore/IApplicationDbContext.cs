﻿using Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading.Tasks;

namespace DataAccess.EntityFrameworkCore
{
    /// <summary>
    /// Defines interface for application db context for testability.
    /// </summary>
    public interface IApplicationDbContext
    {
        /// <summary>
        /// Represents table of transaction records.
        /// </summary>
        DbSet<TransactionRecord> TransactionRecords { get; set; }

        /// <summary>
        /// Save changes into persistent storage.
        /// </summary>
        /// <returns></returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// <see cref="EntityEntry{TransactionRecord}"/>
        /// </summary>
        /// <param name="record"><see cref="TransactionRecord"/></param>
        /// <returns>Return <see cref="EntityEntry{TransactionRecord}"/></returns>
        EntityEntry<TransactionRecord> Entry(TransactionRecord record);
    }
}