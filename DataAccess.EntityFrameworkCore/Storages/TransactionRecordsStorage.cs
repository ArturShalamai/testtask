﻿using Core.Interfaces;
using Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess.EntityFrameworkCore.Storages
{
    /// <summary>
    /// Concrete implementation of <see cref="IStorage{TransactionRecord}"/> for EFCore.
    /// </summary>
    public class TransactionRecordsStorage : IGenericStorage<TransactionRecord>
    {
        /// <summary>
        /// Database context to use.
        /// </summary>
        private readonly IApplicationDbContext _dbContext;

        /// <summary>
        /// Creates a new instance of a class.
        /// </summary>
        /// <param name="dbContext"><see cref="ApplicationDbContext"/></param>
        public TransactionRecordsStorage(IApplicationDbContext dbContext) =>
            this._dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));

        /// <inheritdoc/>
        public async Task<IEnumerable<TransactionRecord>> GetAllAsync(int skip, int limit)
        {
            if (skip == 0 && limit == 0)
            {
                return await this._dbContext.TransactionRecords.ToListAsync();
            }

            return await this._dbContext.TransactionRecords.Skip(skip).Take(limit).AsNoTracking().ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<TransactionRecord>> FindByAsync(Expression<Func<TransactionRecord, bool>> searchCriteria) =>
            await this._dbContext.TransactionRecords.Where(searchCriteria).ToListAsync();

        /// <inheritdoc/>
        public async Task<TransactionRecord> FindOneAsync(Expression<Func<TransactionRecord, bool>> searchCriteria) =>
            await this._dbContext.TransactionRecords.FirstOrDefaultAsync(searchCriteria);

        /// <inheritdoc/>
        public async Task AddAsync(TransactionRecord newRecord) =>
            await this._dbContext.TransactionRecords.AddAsync(newRecord);

        /// <inheritdoc/>
        public async Task AddRangeAsync(IEnumerable<TransactionRecord> newEntities) =>
            await this._dbContext.TransactionRecords.AddRangeAsync(newEntities);

        /// <inheritdoc/>
        public async Task UpdateAsync(TransactionRecord updatedEntity)
        {
            var entity = await this.FindOneAsync(r => r.Id == updatedEntity.Id);

            if (entity is null)
                throw new Exception("No entity to update.");

            // Reason for code is generating sql to update only changed properties.
            if (!string.Equals(entity.Id, updatedEntity.Id))
            {
                entity.Amount = updatedEntity.Amount;
                this._dbContext.Entry(entity).Property("Amount").IsModified = true;
            }

            if (!string.Equals(entity.Amount, updatedEntity.Amount))
            {
                entity.Amount = updatedEntity.Amount;
                this._dbContext.Entry(entity).Property("Amount").IsModified = true;
            }

            if (!string.Equals(entity.CurrencyCode, updatedEntity.CurrencyCode))
            {
                entity.CurrencyCode = updatedEntity.CurrencyCode;
                this._dbContext.Entry(entity).Property("CurrencyCode").IsModified = true;
            }

            if (!entity.TransactionDate.Equals(updatedEntity.TransactionDate))
            {
                entity.TransactionDate = updatedEntity.TransactionDate;
                this._dbContext.Entry(entity).Property("TransactionDate").IsModified = true;
            }

            if (entity.Status != updatedEntity.Status)
            {
                entity.Status = updatedEntity.Status;
                this._dbContext.Entry(entity).Property("Status").IsModified = true;
            }
        }

        /// <inheritdoc/>
        public async Task UpdateRangeAsync(IEnumerable<TransactionRecord> updatedEntities)
        {
            foreach (var entry in updatedEntities)
                await this.UpdateAsync(entry);
        }

        /// <inheritdoc/>
        public async Task RemoveAsync(Expression<Func<TransactionRecord, bool>> removeCriteria)
        {
            var entities = await this._dbContext.TransactionRecords.Where(removeCriteria).ToListAsync();

            if (!entities.Any())
                throw new Exception("No entities to delete.");

            this._dbContext.TransactionRecords.RemoveRange(entities);
        }

        /// <inheritdoc/>
        public async Task SaveChangesAsync() =>
            await this._dbContext.SaveChangesAsync();
    }
}
