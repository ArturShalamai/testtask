﻿using Core.Models;
using DataAccess.EntityFrameworkCore.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading.Tasks;

namespace DataAccess.EntityFrameworkCore
{
    /// <summary>
    /// Application database context.
    /// </summary>
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        /// <inheritdoc>
        public DbSet<TransactionRecord> TransactionRecords { get; set; }

        /// <summary>
        /// Initialize a new instance of a class.
        /// </summary>
        /// <param name="options"><see cref="DbContextOptions{ApplicationDbContext}"/> options.</param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        /// <inheritdoc>
        public EntityEntry<TransactionRecord> Entry(TransactionRecord record) =>
            this.Entry<TransactionRecord>(record);

        /// <inheritdoc>
        public async Task<int> SaveChangesAsync() =>
            await base.SaveChangesAsync();

        /// <summary>
        /// Applies mapping for entities.
        /// </summary>
        /// <param name="modelBuilder"><see cref="ModelBuilder"/></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Add configuration.
            modelBuilder.ApplyConfiguration(new TransactionRecordsConfiguration());
        }
    }
}
