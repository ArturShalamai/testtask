﻿using Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.EntityFrameworkCore.Configurations
{
    /// <summary>
    /// Db Configuration for <see cref="TransactionRecord"/>.
    /// </summary>
    public class TransactionRecordsConfiguration : IEntityTypeConfiguration<TransactionRecord>
    {
        /// <summary>
        /// Define configuration for <see cref="TransactionRecord"/>.
        /// </summary>
        /// <param name="builder"><see cref="EntityTypeBuilder{TransactionRecord}"/></param>
        public void Configure(EntityTypeBuilder<TransactionRecord> builder)
        {
            builder.HasKey(r => r.Id);
            builder.Property(r => r.Id).HasMaxLength(50);

            builder.Property(r => r.Amount).IsRequired().HasColumnType("decimal(19,4)");

            builder.Property(r => r.CurrencyCode)
                .IsRequired()
                .HasMaxLength(3);

            builder.Property(r => r.TransactionDate).IsRequired();

            builder.Property(r => r.Status).IsRequired();
        }
    }
}
